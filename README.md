# README #

## GSON Mapping ##

Berikut bentuk JSON dan Class Mappingnya:
```json
{
    "status": true,
    "user_detail": [
        {
            "id": "1",
            "first_name": "ramdan",
            "last_name": "firdaus",
            "username": "ramdan",
            "password": "47bce5c74f589f4867dbd57e9ca9f808"
        }
    ]
}
```
Mulai dari bagian paling dalam yaitu user_detail


```
#!java

package com.ramdan.user.retrofitexample.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/8/2016.
 */
public class User {
    int id;
    @SerializedName("first_name")
    String firtName;
    @SerializedName("last_name")
    String lastName;
    String username;
    String password;

    public int getId() {
        return id;
    }

    public String getFirtName() {
        return firtName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
```


Kemudian wrapper/pembungkus dari response di atas yang beriisi status dan user_detail


```
#!java
package com.ramdan.user.retrofitexample;

import com.google.gson.annotations.SerializedName;
import com.ramdan.user.retrofitexample.model.User;

import java.util.List;

/**
 * Created by user on 4/8/2016.
 */
public class LoginResponse {
    boolean status;
    @SerializedName("user_detail")
    List<User> userDetail;

    public List<User> getUserDetail() {
        return userDetail;
    }

    public boolean isStatus() {
        return status;
    }


}

```


## API Code ##

API yang digunakan untuk aplikasi ini dapat dililah [disini](https://bitbucket.org/mramdanf/simple-retrofit-api)