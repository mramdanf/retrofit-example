package com.ramdan.user.retrofitexample.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/8/2016.
 */
public class Credential implements Parcelable{
    int id;
    String description;
    @SerializedName("username")
    String credUsername;
    @SerializedName("password")
    String credPassword;

    // ============== Parcelabel implementation ============
    protected Credential(Parcel in) {
        id = in.readInt();
        description = in.readString();
        credUsername = in.readString();
        credPassword = in.readString();
    }

    public static final Creator<Credential> CREATOR = new Creator<Credential>() {
        @Override
        public Credential createFromParcel(Parcel in) {
            return new Credential(in);
        }

        @Override
        public Credential[] newArray(int size) {
            return new Credential[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(description);
        dest.writeString(credUsername);
        dest.writeString(credPassword);
    }

    // ============== Parcelabel implementation ============

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getCredUsername() {
        return credUsername;
    }

    public String getCredPassword() {
        return credPassword;
    }


}
