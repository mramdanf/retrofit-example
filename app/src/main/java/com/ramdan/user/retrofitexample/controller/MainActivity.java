package com.ramdan.user.retrofitexample.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.ramdan.user.retrofitexample.apiendpoint.ApiEndpoint;
import com.ramdan.user.retrofitexample.response.LoginResponse;
import com.ramdan.user.retrofitexample.R;
import com.ramdan.user.retrofitexample.helper.Utility;
import com.ramdan.user.retrofitexample.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private EditText mUsernameEditText;
    private EditText mPasswordEditText;
    private LoginResponse apiLoginResponse;
    private User user;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUsernameEditText = (EditText) findViewById(R.id.username);
        mPasswordEditText = (EditText) findViewById(R.id.password);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");

        if (Utility.getUserDetailPrf(this).contains(Utility.KEY_USERNAME)){
            startActivity(new Intent(this, ListCredActivity.class));
            finish();
        }
    }

    public void doLogin(View view) {
        final String username = mUsernameEditText.getText().toString();
        final String password = mPasswordEditText.getText().toString();


        ApiEndpoint apiEndpoint = Utility.getClient().create(ApiEndpoint.class);
        Call<LoginResponse> call = apiEndpoint.loginService(username, password);
        progressDialog.show();
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressDialog.dismiss();
                apiLoginResponse = response.body();
                if (apiLoginResponse != null){
                    if (apiLoginResponse.getUserDetail().size() > 0){
                        user = apiLoginResponse.getUserDetail().get(0);

                        SharedPreferences.Editor editor = Utility.getUserDetailEditor(MainActivity.this);
                        editor.putString(Utility.KEY_USERNAME, username);
                        editor.putString(Utility.KEY_PASSWORD, password);
                        editor.apply();

                        startActivity(new Intent(MainActivity.this, ListCredActivity.class));
                        finish();

                    } else Log.d("MainActivity", "apiloginresponse size < 0");

                }else Utility.displayAlert(MainActivity.this, "Login gagal, username atau password salah.");
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });


    }
}
