package com.ramdan.user.retrofitexample.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Base64;
import android.widget.Toast;

import com.ramdan.user.retrofitexample.R;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 4/8/2016.
 */
public class Utility {

    private static Retrofit retrofit = null;

    public static final String PRF_USER_DETAIL = "user-detail";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";
    private static final String BASE_URL = "http://10.99.226.60/simple_retrofit/";
//    private static final String BASE_URL = "http://192.168.1.100/simple_retrofit/";
    public static final String KEY_CRED_PARCEL = "cred-parcel";

    public static final int REQ_ADD_CRED = 1;


    public static Retrofit getClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public static Retrofit getClientWithBasicAuth(final String username, final String password){

        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                String credentials = username + ":" + password;
                final String basic =
                        "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                Request newRequest = chain.request().newBuilder().addHeader("Authorization", basic).build();

                return chain.proceed(newRequest);
            }
        };

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(interceptor);
        builder.addNetworkInterceptor(httpLoggingInterceptor);
        OkHttpClient client = builder.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;
    }

    public static void displayLongToast(Context c, String msg){
        Toast.makeText(c, msg, Toast.LENGTH_LONG).show();
    }

    public static void displayAlert(Context c, String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setMessage(msg)
                .setTitle(c.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                                dialog.cancel();
                            }
                        })

                .show();
    }

    public static SharedPreferences getUserDetailPrf(Context c){
        return c.getSharedPreferences(Utility.PRF_USER_DETAIL, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getUserDetailEditor(Context c){
        return c.getSharedPreferences(Utility.PRF_USER_DETAIL, Context.MODE_PRIVATE).edit();
    }

}
