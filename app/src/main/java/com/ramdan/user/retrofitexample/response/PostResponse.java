package com.ramdan.user.retrofitexample.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/8/2016.
 */
public class PostResponse {
    boolean status;
    @SerializedName("post_msg")
    String postMsg;

    public String getPostMsg() {
        return postMsg;
    }

    public boolean isStatus() {
        return status;
    }
}
