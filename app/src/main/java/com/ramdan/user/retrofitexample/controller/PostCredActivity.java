package com.ramdan.user.retrofitexample.controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ramdan.user.retrofitexample.apiendpoint.ApiEndpoint;
import com.ramdan.user.retrofitexample.response.PostResponse;
import com.ramdan.user.retrofitexample.R;
import com.ramdan.user.retrofitexample.helper.Utility;
import com.ramdan.user.retrofitexample.model.Credential;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostCredActivity extends AppCompatActivity {

    private final String LOG_TAG = "PostCredActivity";
    private Credential credential;
    private EditText descriptionEditText;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button postButton;

    private ProgressDialog progressDialog;
    int idCred;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_cred);


        descriptionEditText = (EditText) findViewById(R.id.cred_desc);
        usernameEditText = (EditText) findViewById(R.id.cred_username);
        passwordEditText = (EditText) findViewById(R.id.cred_password);
        postButton = (Button) findViewById(R.id.post_cred_button);

        postButton.setText(R.string.text_save);

        idCred = 0;

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");

        Intent i = getIntent();
        if (i.hasExtra(Utility.KEY_CRED_PARCEL)){
            postButton.setText(getString(R.string.text_update));

            credential = i.getParcelableExtra(Utility.KEY_CRED_PARCEL);
            descriptionEditText.setText(credential.getDescription());
            usernameEditText.setText(credential.getCredUsername());
            passwordEditText.setText(credential.getCredPassword());

            idCred = credential.getId();
        }
    }

    public void postCred(View view) {
        String username = Utility.getUserDetailPrf(this).getString(Utility.KEY_USERNAME, "");
        String password = Utility.getUserDetailPrf(this).getString(Utility.KEY_PASSWORD, "");

        String desc = descriptionEditText.getText().toString();
        String user = usernameEditText.getText().toString();
        String pass = passwordEditText.getText().toString();


        ApiEndpoint apiEndpoint = Utility.getClientWithBasicAuth(username, password)
                .create(ApiEndpoint.class);
        progressDialog.show();
        Call<PostResponse> call = apiEndpoint.postCredService(
                idCred,
                desc,
                user,
                pass
        );

        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                progressDialog.dismiss();
                PostResponse postResponse = response.body();
                if (postResponse.isStatus()) {
                    setResult(Activity.RESULT_OK, getIntent());
                    finish();
                }
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }
}
