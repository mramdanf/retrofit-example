package com.ramdan.user.retrofitexample.response;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ramdan.user.retrofitexample.R;
import com.ramdan.user.retrofitexample.model.Credential;

import java.util.List;

/**
 * Created by user on 4/8/2016.
 */
public class CredentialsAdapter
        extends RecyclerView.Adapter<CredentialsAdapter.CredentialViewHolder> {

    private List<Credential> credentials;

    public static class CredentialViewHolder extends RecyclerView.ViewHolder {
        TextView descTextView;
        TextView usernameTextView;
        TextView passwordTextView;

        public CredentialViewHolder(View v) {
            super(v);
            descTextView = (TextView) v.findViewById(R.id.description);
            usernameTextView = (TextView) v.findViewById(R.id.username);
            passwordTextView = (TextView) v.findViewById(R.id.password);
        }
    }

    public CredentialsAdapter(List<Credential> credentials) {
        this.credentials = credentials;
    }

    @Override
    public CredentialViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_cred_item, parent, false);
        return new CredentialViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CredentialViewHolder holder, int position) {
        holder.descTextView.setText(credentials.get(position).getDescription());
        holder.usernameTextView.setText(credentials.get(position).getCredUsername());
        holder.passwordTextView.setText(credentials.get(position).getCredPassword());
    }

    @Override
    public int getItemCount() {
        return credentials.size();
    }
}
