package com.ramdan.user.retrofitexample.response;

import com.google.gson.annotations.SerializedName;
import com.ramdan.user.retrofitexample.model.User;

import java.util.List;

/**
 * Created by user on 4/8/2016.
 */
public class LoginResponse {
    boolean status;
    @SerializedName("user_detail")
    List<User> userDetail;

    public List<User> getUserDetail() {
        return userDetail;
    }

    public boolean isStatus() {
        return status;
    }


}
