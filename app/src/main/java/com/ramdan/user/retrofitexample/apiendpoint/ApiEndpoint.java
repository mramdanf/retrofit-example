package com.ramdan.user.retrofitexample.apiendpoint;

import com.ramdan.user.retrofitexample.response.CredentialResponse;
import com.ramdan.user.retrofitexample.response.LoginResponse;
import com.ramdan.user.retrofitexample.response.PostResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by user on 4/8/2016.
 */
public interface ApiEndpoint {

    @FormUrlEncoded
    @POST("account/auth")
    Call<LoginResponse> loginService(
            @Field("username") String username,
            @Field("password") String password

    );

    @GET("credentials/get")
    Call<CredentialResponse> getCredentialsData();


    @FormUrlEncoded
    @POST("credentials/post")
    Call<PostResponse> postCredService(
            @Field("id") int id,
            @Field("description") String description,
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("credentials/delete")
    Call<PostResponse> deleteCredService(
            @Field("id") int id
    );

}
