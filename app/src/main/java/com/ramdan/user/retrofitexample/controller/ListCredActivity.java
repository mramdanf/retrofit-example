package com.ramdan.user.retrofitexample.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ramdan.user.retrofitexample.apiendpoint.ApiEndpoint;
import com.ramdan.user.retrofitexample.response.CredentialResponse;
import com.ramdan.user.retrofitexample.response.CredentialsAdapter;
import com.ramdan.user.retrofitexample.response.PostResponse;
import com.ramdan.user.retrofitexample.R;
import com.ramdan.user.retrofitexample.helper.Utility;
import com.ramdan.user.retrofitexample.helper.RecyclerItemClickListener;
import com.ramdan.user.retrofitexample.model.Credential;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListCredActivity extends AppCompatActivity
    implements RecyclerItemClickListener.OnItemClickListener{

    // this is mark for commit revert test

    private ProgressDialog progressDialog;
    private CredentialResponse credentialResponse;
    private final String LOG_TAG = "ListCredActivity";
    private RecyclerView mRecylerView;
    private List<Credential> credentialList;
    private CredentialsAdapter credentialsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cred);


        credentialList = new ArrayList<>();
//        credentialsAdapter = new CredentialsAdapter(credentialList);

        mRecylerView = (RecyclerView) findViewById(R.id.creds_recycler_view);
//        mRecylerView.setAdapter(credentialsAdapter);
        mRecylerView.setLayoutManager(new LinearLayoutManager(this));
        mRecylerView.addOnItemTouchListener(new RecyclerItemClickListener(this, this));


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");

        loadDataCredentials();
    }

    private void loadDataCredentials(){
        String username = Utility.getUserDetailPrf(this).getString(Utility.KEY_USERNAME, "");
        String password = Utility.getUserDetailPrf(this).getString(Utility.KEY_PASSWORD, "");

        ApiEndpoint apiEndpoint = Utility.getClientWithBasicAuth(username, password)
                .create(ApiEndpoint.class);
        Call<CredentialResponse> call = apiEndpoint.getCredentialsData();
        progressDialog.show();
        call.enqueue(new Callback<CredentialResponse>() {
            @Override
            public void onResponse(Call<CredentialResponse> call, Response<CredentialResponse> response) {
                credentialResponse = response.body();
                progressDialog.dismiss();
                credentialList.clear();
                if (credentialResponse != null){

                    credentialList = credentialResponse.getCredentials();


                }else Log.d("ListCredActivity", "credential response was null");

                mRecylerView.setAdapter(new CredentialsAdapter(credentialList));
            }

            @Override
            public void onFailure(Call<CredentialResponse> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.list_cred_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_logout:
                Utility.getUserDetailEditor(this).clear().commit();
                startActivity(new Intent(this, MainActivity.class));
                finish();
                return true;
            case R.id.action_add_cred:
                startActivityForResult(new Intent(this, PostCredActivity.class), Utility.REQ_ADD_CRED);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(View childView, int position) {
        goDetail(position);
    }

    @Override
    public void onItemLongPress(View childView, final int position) {

        String listOptions[] = new String[] {"Delete data", "View data"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Action");
        builder.setItems(listOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        deleteCred(credentialList.get(position).getId());
                        break;
                    case 1:
                        goDetail(position);
                        break;
                }
            }
        });
        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case Utility.REQ_ADD_CRED:
                switch (resultCode){
                    case Activity.RESULT_OK:
                        loadDataCredentials();
                        break;
                }
                break;
        }
    }

    private void goDetail(int position){
        if (credentialList != null){
            Credential credential = credentialList.get(position);
            Intent i = new Intent(this, PostCredActivity.class);
            i.putExtra(Utility.KEY_CRED_PARCEL, credential);
            startActivityForResult(i, Utility.REQ_ADD_CRED);
        }
    }

    private void deleteCred(int idCred){
        String username = Utility.getUserDetailPrf(this).getString(Utility.KEY_USERNAME, "");
        String password = Utility.getUserDetailPrf(this).getString(Utility.KEY_PASSWORD, "");

        ApiEndpoint apiEndpoint = Utility.getClientWithBasicAuth(username, password)
                .create(ApiEndpoint.class);
        progressDialog.show();
        Call<PostResponse> call = apiEndpoint.deleteCredService(idCred);
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                progressDialog.dismiss();
                PostResponse postResponse = response.body();
                if (postResponse.isStatus()){
                    loadDataCredentials();
                }
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
