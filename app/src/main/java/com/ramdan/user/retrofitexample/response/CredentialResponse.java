package com.ramdan.user.retrofitexample.response;

import com.google.gson.annotations.SerializedName;
import com.ramdan.user.retrofitexample.model.Credential;

import java.util.List;

/**
 * Created by user on 4/8/2016.
 */
public class CredentialResponse {
    boolean status;

    @SerializedName("credential_data")
    List<Credential> credentials;

    public boolean isStatus() {
        return status;
    }

    public List<Credential> getCredentials() {
        return credentials;
    }
}
