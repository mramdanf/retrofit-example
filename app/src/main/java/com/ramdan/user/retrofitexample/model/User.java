package com.ramdan.user.retrofitexample.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/8/2016.
 */
public class User {
    int id;
    @SerializedName("first_name")
    String firtName;
    @SerializedName("last_name")
    String lastName;
    String username;
    String password;

    public int getId() {
        return id;
    }

    public String getFirtName() {
        return firtName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
